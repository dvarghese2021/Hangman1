package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here

        boolean play = false;
        String repeat = "";
        Scanner input = new Scanner(System.in);

        do{
            hangman();
            System.out.println("Would you like to play again? (y or n)");
            repeat = input.next();

            if (repeat.charAt(0) == 'y'){
                play = true;
                System.out.println("\033[0;1m" + repeat);
            }else {
                play = false;
                System.out.println("\033[0;1m" + repeat);
            }
        }while(play);


    }
    public static void hangman(){

        ArrayList<String> str1 = new ArrayList<>();
        str1.add("one");
        str1.add("man");
        str1.add("cat");
        str1.add("dog");
        str1.add("God");

        int count =0;
       //int rad = (int) (Math.random()*(str1.size()-1+1)+1);

        ArrayList<String> str2 = new ArrayList<>();

        String guess = "";
        Scanner input1 = new Scanner(System.in);

        System.out.println("+-------------+");
        System.out.println("   |          ");
        System.out.println("   |           ");
        System.out.println("   |       ");
        System.out.println("   |          ");
        System.out.println("   |       ");
        System.out.println("   |");
        System.out.println("   |");
        System.out.println("___|___");

        System.out.println("Missed letters:");

        for(int i=0; i< str1.get(0).length(); i++){
            str2.add("_");
            System.out.print("_");

        }
        System.out.println("");




        int n= 0;
        int num =0;
            while (count < 4) {
                System.out.println("");
                System.out.println("Guess a letter.");
                guess = input1.next();
                for (int k=0; k<str2.size(); k++){
                    if (str2.get(k).equals(guess)){
                        System.out.println("You have already guessed that letter. Choose again");
                        break;
                    }
                }
                for (int j = num; j < str1.get(0).length(); j++) {


                    if (str1.get(0).charAt(j) == guess.charAt(0)) {
                        str2.set(j, guess);
                        num++;
                        System.out.println(str2);
                        break;

                    } else{

                        System.out.println("Missed letters: " + guess);
                        count++;
                        switch (count) {

                            case 1:
                                System.out.println("+-------------+");
                                System.out.println("   |          0");
                                System.out.println("   |          |");
                                System.out.println("   |       ");
                                System.out.println("   |          ");
                                System.out.println("   |        ");
                                System.out.println("   |");
                                System.out.println("   |");
                                System.out.println("___|___");

                                break;

                            case 2:

                                System.out.println("+-------------+");
                                System.out.println("   |          0");
                                System.out.println("   |          |");
                                System.out.println("   |       //___\\\\");
                                System.out.println("   |          ");
                                System.out.println("   |        ");
                                System.out.println("   |");
                                System.out.println("   |");
                                System.out.println("___|___");
                                break;

                            case 3:
                                System.out.println("+-------------+");
                                System.out.println("   |          0");
                                System.out.println("   |          |");
                                System.out.println("   |       //___\\\\");
                                System.out.println("   |          |");
                                System.out.println("   |       ");
                                System.out.println("   |");
                                System.out.println("   |");
                                System.out.println("___|___");
                                break;

                            case 4:
                                System.out.println("+-------------+");
                                System.out.println("   |          0");
                                System.out.println("   |          |");
                                System.out.println("   |       //___\\\\");
                                System.out.println("   |          |");
                                System.out.println("   |        //\\\\");
                                System.out.println("   |");
                                System.out.println("   |");
                                System.out.println("___|___");
                                break;
                        }
                        break;

                    }

                }
                if (num > str2.get(0).length()+1){
                    System.out.println("Yes! secret word is " + str2 + " ! You have won!");
                    break;
                }
            }

        }








}
